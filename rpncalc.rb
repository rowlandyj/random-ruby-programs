class RPNCalculator
	def evaluate(rpn)
       @stack = []
       
       rpn.split(' ').each {|x|
       	
       	if x =~ /\d/
       	  @stack << x.to_i        
       	elsif x == '+'
       	  @stack << @stack.pop + @stack.pop
       	elsif x == '*'
       	  @stack << @stack.pop * @stack.pop
       	elsif x == '-'
       	  @stack << -@stack.pop + @stack.pop
              elsif x == '/'
                a = @stack.pop
                b = @stack.pop
                @stack << (b / a)
       	end		}
       
       return @stack[0] 
	end
end

calc = RPNCalculator.new

puts calc.evaluate('1 2 +')
puts '----'
puts calc.evaluate('3 2 *')
puts '----'
puts calc.evaluate('1 2 -')
puts '----'
puts calc.evaluate('5 1 2 + 4 * + 3 -')
puts '----'
puts calc.evaluate('70 10 4 + 5 * -')
puts '---'
puts calc.evaluate(["10","6","9","3","+","-11","*","/","*","17","+","5","+"].join(" "))