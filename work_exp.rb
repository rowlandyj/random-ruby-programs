require 'date'

def work_exp(input)
    dates = parse_input(input)
    dates = overlap(dates)
    num_years(dates)
end

def parse_input(input)
    arr = input.chomp.split("; ")
    dates = arr.map { |x| x.split("-") }

    dates.each_index do |x| 
        dates[x][0] = Date.parse(dates[x][0])
        dates[x][1] = Date.parse(dates[x][1])
    end

    dates.sort
end

def overlap(arr)
    result = []
    overlap_index = []

    arr.each_index do |i|
        if (i == arr.length - 1) && !overlap_index.include?(i)
            result << arr[i]
            break
        end

        start_a = arr[i][0]
        end_a = arr[i][1]
        
        (i + 1).upto(arr.length - 1) do |x|
            break if i == arr.length - 1
            start_b = arr[x][0]
            end_b = arr[x][1]
            if (start_a <= end_b) && (end_a >= start_b)
                a = [start_a, start_b, end_a, end_b].sort
                result << [a.first, a.last]
                overlap_index << i
                overlap_index << x
            else
                next
            end
        end
        overlap_index.include?(i) ? nil : result << arr[i]
    end

    return result
end

def num_years(dates)
    total = 0
    dates.each do |x|
        start = x.first
        fin = x.last
        year = fin.year - start.year
        if fin.mon < start.mon
            year -= 1
            month = fin.mon - start.mon + 12
        else
            month = fin.mon - start.mon + 1
        end
        total += year * 12 + month
    end
    return total / 12
end

puts work_exp("Feb 2004-Dec 2009; Sep 2004-Jul 2008")
puts work_exp("Aug 2013-Mar 2014; Apr 2013-Aug 2013; Jun 2014-Aug 2015; Apr 2003-Nov 2004; Apr 2014-Jan 2015")
puts work_exp("Mar 2003-Jul 2003; Nov 2003-Jan 2004; Apr 1999-Nov 1999")
puts work_exp('Apr 1992-Dec 1993; Feb 1996-Sep 1997; Jan 2002-Jun 2002; Sep 2003-Apr 2004; Feb 2010-Nov 2011')
puts work_exp("Feb 2004-May 2004; Jun 2004-Jul 2004")