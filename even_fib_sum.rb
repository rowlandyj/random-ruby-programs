def fib(n)
	n < 2 ? n : fib(n-1) + fib(n-2)
end

puts fib(6)

def even_fib_sum(max)
	sum = 0
	i = 1

	while fib(i) < max
		if fib(i) != 0 && fib(i) % 2 ==0
			sum += fib(i)
		end

		i += 1
	end

	return sum
end

puts even_fib_sum(4000000)