def call_num(file_name)
	file = open(file_name)
	number = file.read
ensure
	file.close if file
	return number
end

def digit_product(file_name)
	number = call_num(file_name)
	number.delete("\n")
	num_len = number.length
	prod = 1 
	great_prod = 1
	i = 0

	while (i + 5) < num_len
		if (number[i..(i+4)].include? "0") == false
			number[i..(i+4)].each_char { |x| prod = prod * x.to_i }
		end
		
		if great_prod < prod
			great_prod = prod
		end

		prod = 1
		i += 1
	end
	
	return great_prod
end

puts digit_product("bignumber.txt")
puts 9**5