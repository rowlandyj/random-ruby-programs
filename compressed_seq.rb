def compressed_seq(input)
    result = []
    arr = input.chomp.split(" ").map(&:to_i)
    
    count = 0
    prev = arr[0]
    arr.each do |x|
        if prev == x
            count += 1
        elsif prev != x
            result << count
            result << prev
            count = 1
        end
        prev = x
    end
    result << count
    result << arr[-1]

    result.join(" ")
end

puts compressed_seq("40 40 40 40 29 29 29 29 29 29 29 29 57 57 92 92 92 92 92 86 86 86 86 86 86 86 86 86 86")
puts compressed_seq("73 73 73 73 41 41 41 41 41 41 41 41 41 41")
puts compressed_seq("1 1 3 3 3 2 2 2 2 14 14 14 11 11 11 2")
puts compressed_seq("7")