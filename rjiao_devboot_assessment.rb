#**Please take note that I have commented out some sections of code that I used for debug/testing.  

#Ex 1
home_address = "18420 Couples Court"
home_city = "Yorba Linda"
home_state = "CA"

#Ex 2
=begin
  	'=' assigns a value to an object.  
  	**The left of '=' gets assigned the value to the right of '='  
  	  Example 1: a = 2, variable a is assigned to the int value 2
  	  Example 2: b = "Hello World", variable b is assigned to the string value "Hello World"
  	'==' compares the values of 2 objects.
  	  Example 1: a = 1, b = 1,  a == b --> true. the vars a and b are assigned the same int value.
  	    The value of a compared to the value of b is the same hence true.
  	  Example 1: a = 2, b = 1,  a == b --> false. the vars a and b are assigned different int values.
  	    The value of a compared to the value of b is not the same hence false.  

  	So for the first two lines of code we have a variable first_name being assigned the string value of "Anne".
  	In the second line of code we are comparing the value of the variable first_name to the string "Anee".
  	Since the two are equal in value, the second line of code should result in true.

    If one were to run the last two lines of code one would run into an error.  If one were to swap the items on the left side
    of '=' with the things on the right side so that it reads: 
    		first_name = "Spalding" 
    		first_name == "Spalding"
    The code would run properly and behave similar to the first 2 lines of code.  Also if the last two lines of code could run properly
    that would mean that one could reassign "definitive-valued" objects to mean something else which could make coding extremely painful
    to read and debug.  One would also run into "class clashing" issues (ie Int methods do not work on String or vice versa).
    The computer would have problems interpreting the code.  Think what would happen if you assigned
    the string "Hello World" to the int 5 ("Hello World" = 5).  The whole world could explode...or at least be very very confused at the very least.    
=end

#Ex 3
=begin
  **the first item in an array has and index value of 0
  index at 3: "best"
  index of "ruby": 0
  length of array: 7
  **the code below can be used to find the answers to the questions in this exercise	
=end
arr = ["ruby", "is", "the", "best", "programming", "language", "ever"]
puts arr[3]
puts arr.index("ruby")
puts arr.length

puts '----------------'
#Ex 4
def product (array)
	return array.inject(:*)
end

#checking to see if code works properly
puts product([1,2,3])    
puts product([0,-1,-10]) 
puts product([1,-1,-10])
puts "----------------"


#Ex 5
def product_odd (array)
	return array.reject { |x| x % 2 == 0 }.inject(:*)
end

#checking to see if code works properly
puts product_odd([1,2,3])    
puts product_odd([0,-1,-10]) 
puts product_odd([1,2,3,4,5])

puts '----------------'

#Ex 6
=begin 
  I used puts to print out the numbers/string so that each one will be on a new line to make it easier to read.
  One could just use print instead but it would be hard to read unless you add spacing to each one. 
  (will have to change n to n.to_s + ' ' for the last part or use "#{n} ").
  I also made the method have a optional input (must be a positive integer) so one can increase or decrease the range.
=end
def fizzblam (max=1000)
  1.upto(max) do |n|
  	if n % 5 == 0 and n % 7 == 0
  		puts "FizzBlam"
  	elsif n % 5 == 0
  		puts "Fizz"
  	elsif n % 7 == 0
  		puts "Blam"
  	else
  	  	puts n
  	end
  end
end
#uncomment below to see the method in action.  I uncommented it so my console would not be so cluttered.
#fizzblam

puts '----------------'

#Ex 7
=begin
  method_one: returns the biggest value in the array by comparing each value in the array and using the first value as the starting point.
  method_two: returns all values in the array that is greater than 0 in a new array.
  method_three: returns all even values in the array in a new array.
=end

def method_three(array)
	return array.select { |x| x.even? } 
end

#Ex 8
=begin
	So there's two main things that are different between puts and return:  
	
	Return exits out of a loop early whereas puts ur stuck until the exit condition (hopefully there is one!) is fully met in the loop.  
	Also please note that return does not mean print. So depending on how you write your code, you may need to include a puts (method) 
	to see the results of your method.  Also remember return is implied so you may not need to write it.  
	I do to make sure it is clear on what I am returning  
	
	Puts prints everything out as a string.  It is easy to forget this since puts evaluates the expression before hand. So puts 3 + 7 
	is actually printing out "10" and not the Int 10.  

	Below are some example codes showing the two in action.	
=end

def exit_early
	1.upto(5) do |x|
		return 'exited early' if x == 3
		puts x
	end
end

puts exit_early
puts '------------'

def exit_at_end
	1.upto(5) do |x|
		puts 'exited' if x==3
		puts x
	end
end
exit_at_end
puts '------------'

#Note how the 3 is not printed in the exit_early method whereas the second one prints the message and still keeps on evaluating.
#That is because the in the first method the loop is exited before finishing.

def using_puts
	puts 2+3
end

def using_return
	return 2+3
end

puts 3+5 #this is just to show that the expression after puts is evaluated before printing
puts '----------------'
puts using_return + 3 #everything works fine, no error :)
#puts using_puts + 3 will print out 5 but will not evaluate, you get an error instead :(.  This will stop the rest of code so I commented it out.
puts '----------------'
#Also please note
using_return #does not print but is evaluated
proof = using_return
puts proof #aha! here is proof!!
puts '----------------'
using_puts #prints and is evaluted
puts '----------------'

#Ex 9
=begin
  The easiest way I can think of doing this is to just generate a complete list of values 1..10,000
  and then compare the two list for the missing value.  To do this I can use - to get the missing value 
  since it removes duplicates.  Please note using - returns another array
  
  I wrote the method with two inputs.  The first is the actual list of numbers, and the second is the max
  value of list. In this problem the max is 10,000 which is the default that I gave for the second input.
  To run my method for this problem you only need to input the list of 1..10000 (minus 1 value) to run it properly.
  
  **Took about 6 minutes to think and type out.   
=end

def missing_num (list, max=10000)
	arr = []
	1.upto(max) do |i|
		arr << i
	end
	return arr - list
end

#Ex 10
=begin
  *Goal is to return a Hash.  Assuming the input is not an array (hence the splat operator).
  def grocery_list (*item)
  	return Hash[*item.group_by{ block.to_sym }.flat_map{ block }]
  end

  **took about 15 minutes to figure out and type out psuedo code then about 5 to clean up the actual code.  Had to google and experiment with .group_by enumerable.
  **I was really confused by what the problem wanted at first but I figured it out eventually...now I just feel silly for not understanding in the first place.   
=end

def grocery_list (*item)
  	list = item.group_by{ |i| i.to_sym } #.to_sym is unnecessary but it looks nicer to me.  
  	list.map { |k,v| list[k]=v.length }
  	return list.empty? ? "No items on your grocery list.  I suggest adding cake to start." : list
end


=begin
*Below is testing/debugging code
puts grocery_list("milk", "milk", "cake", "cake", "cake", "cake", "bacon")
puts grocery_list
=end

#Ex 11
=begin
	class Home
		def initialize ( temp = 70, min_temp = 60, max_temp = 90)
			set instant variables to the respective input value
			heater and air_con default to off/false
		end

		def heater (switch)
			case @temp
				when on
					@heater = true
				when off
					@heater false
				else
					error message if no string of 'on' or 'off'
			end
		end

		def air_con (switch)
			case @temp
				when on
					@air_con = true
				when off
					@air_con = false
				else
					error message if no string of 'on' or 'off'
			end
		end

		def update_temperature!
			if heater
				@temp += 1
			elsif air_con
				@temp -= 2
			end

			puts current temperature 

			if too hot
				extreme temp message
				heater = false, air_con = true
			elsif too cold
				extreme temp message
				air_con = false, heater = true
			end
		end
	end


	*about 8 minutes to think out and write out psuedo code and about 10 minutes of writing the actual code and fixing bugs
=end

class Home
	def initialize ( temp = 70, min_temp = 60, max_temp = 90 )
		@temp = temp
		@min_temp = min_temp
		@max_temp = max_temp
		@heater = false
		@air_con = false
	end

	def update_temperature!

		if @heater
			@temp += 1
		elsif @air_con
			@temp -= 2
		end

		puts "Current Temperature is #{@temp} F"

		if @temp >= @max_temp
			@air_con = true, @heater = false
			puts "Temperature is too high in the house."
			puts "The air conditioner is now on."
		elsif @temp <= @min_temp
			@air_con = false, @heater = true
			puts "Temperature is too low in the house."
			puts "The heater is now on."
		end
	end

	def heater (switch)
		case switch.downcase
		when "on" 
			@heater = true, @air_con = false
		when "off" 
			@heater = false
		else
			puts "That is an invalid command.  Please indicate if you want the heater \"on\" or \"off.\""
		end
	end

	def air_con (switch)
		case switch.downcase
		when "on" 
			@air_con = true, @heater = false
		when "off" 
			@air_con = false
		else
			puts "That is an invalid command.  Please indicate if you want the air conditioning \"on\" or \"off.\""
		end
	end
end


=begin
*Below is the code I used for testing/debugging
house = Home.new(62)
haus = Home.new(89)
casa = Home.new(61)
house.air_con("on")
house.heater("on")
house.update_temperature!
house.update_temperature!
house.update_temperature!
puts '--------------'
haus.heater("on")
haus.update_temperature!
haus.update_temperature!
haus.update_temperature!
puts '--------------'
casa.air_con("on")
casa.update_temperature!
casa.update_temperature!
casa.update_temperature!
=end

#Ex 12
=begin

**HTML and CSS took about 3 minutes.  
**For the javascript part took about 30 minutes (tried to learn through codeacademy at first but thought it be faster to look up the specific action I wanted via google)
**I will continue working on finishing the rest of the codeacademy course to get a better understanding of javascript.

JS fiddle link: http://jsfiddle.net/beWTC/
=end