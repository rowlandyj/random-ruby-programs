def factorial(n)
	n == 0 ? 1 : n * factorial(n-1)
end

# p factorial(3) == 6
# p factorial(4) == 24
# p factorial(5) == 120 

def choose_team(n,k)
	factorial(n)/(factorial(k)*factorial(n-k))
end

# p choose_team(6,2) == 15
# p choose_team(6,3) == 20
# p choose_team(24,4) == 10626

#orrr in one method only

def choose_team2(n,k)
return n if k == 1 
return 0 if n == 0
choose_team(n-1,k-1) + choose_team(n-1,k)
end

p choose_team2(6,2) == 15
p choose_team2(6,3) == 20
p choose_team2(24,4) == 10626
