def swap_elem(input)
    arr = input.chomp.split(" : ")
    list = arr[0].split(" ")
    swaps = arr[1].split(", ")

    swaps.each do |x|
        dx = x.split("-")
        i = dx[0].to_i
        j = dx[1].to_i
        swap = list[i]
        list[i] = list[j]
        list[j] = swap
    end

    return list.join(" ")
end

puts swap_elem("1 2 3 4 5 6 7 8 9 : 0-8") == "9 2 3 4 5 6 7 8 1"
puts swap_elem("1 2 3 4 5 6 7 8 9 10 : 0-1, 1-3") == "2 4 3 1 5 6 7 8 9 10"