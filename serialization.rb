require 'csv'
require 'date'

class Person
  attr_reader :id, :first_name, :last_name, :email, :phone, :created_at

  def initialize (row)
    @id = row[0]
    @first_name = row[1]
    @last_name = row[2]
    @email = row[3]
    @phone = row[4]
    @created_at = row[5]
  end

  def to_a
    [@id,@first_name,@last_name,@email,@phone,@created_at]
  end

end
 
class PersonParser
  attr_reader :file
  
  def initialize(file)
    @file = file
    @people = nil
  end
  
  def people
    return @people if @people
    CSV.foreach(@file) do |row|
      @people ||= []
      @people << Person.new(row).to_a
    end
    @people.delete_at(0)
  end

  def add_person(person)
    @people << person
  end

  def save
    CSV.open('parsedcsv.csv', 'w+') do |csv|
      csv << ['id','first_name','last_name','email','phone','created_at']
      @people.each do |person|
        csv << person.to_a
      end
    end
  end
end
 
parser = PersonParser.new('people.csv')
parser.people
parser.save

puts "There are #{parser.people.size} people in the file '#{parser.file}'."

