def get_grid(file_name)
	require "matrix"

	file = open(file_name)
	grid = file.read
	mat = Matrix.rows(grid.lines.map { |x| x.split })
ensure
	file.close if file
	return mat
end

def prod_grid(file_name)	
	grid = get_grid(file_name)
end

puts prod_grid("num_grid.txt")