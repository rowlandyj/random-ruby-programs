class GuessingGame
  def initialize(guess)
    @answer = guess
    @guess  = rand
  end
  
  def guess(guess)
    @guess = guess
    if @guess < @answer
      return :low
    elsif @guess > @answer
      return :high
    else
      return :correct
    end
  end
  
  def solved?
    @guess == @answer ? true:false
  end

  def answer
    @answer 
  end
end

game = GuessingGame.new(0)

puts game.solved?   # => false

puts game.guess(5)  # => :low
puts game.guess(20) # => :high
puts game.solved?   # => false

puts game.guess(10) # => :correct
puts game.solved?   # => true
puts rand
puts rand == 1