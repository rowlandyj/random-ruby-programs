def road_trip(input)
    start = 0
    result = []
    str = input.gsub(/[a-zA-Z,]/,"")
    arr = str.split(";")
    arr.map!(&:to_i)
    arr.sort.each do |x|
        result << (x - start)
        start = x
    end
    result.join(",")
end

puts road_trip("Rkbs,5453; Wdqiz,1245; Rwds,3890; Ujma,5589; Tbzmo,1303;") == "1245,58,2587,1563,136"
puts road_trip("Vgdfz,70; Mgknxpi,3958; Nsptghk,2626; Wuzp,2559; Jcdwi,3761;") == "70,2489,67,1135,197"
puts road_trip("Yvnzjwk,5363; Pkabj,5999; Xznvb,3584; Jfksvx,1240; Inwm,5720;") == "1240,2344,1779,357,279"