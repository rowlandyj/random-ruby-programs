# Takes as its input an integer +n+ representing the starting number of bottles
# Prints out the lyrics to "n Bottles of Beer" to the console.
 
def bottle_song(n)
	return puts "1 bottle of beer on the wall, 1 bottle of beer. \nTake one down, pass it around, no more bottles of beer on the wall!" if n == 1
	
	puts "#{n} bottles of beer on the wall, #{n} bottles of beer. \nTake one down, pass it around, #{n-1} bottles of beer on the wall!"
    
    bottle_song(n-1)
end

puts bottle_song(5)