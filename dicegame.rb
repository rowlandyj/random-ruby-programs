class Die
	
	def roll
		rand(1..6)
	end
end

def score (dice_rolls=[])
	total = 0
	
	if dice_rolls.length > 5
		return "too many rolls"
	end 
	
	if dice_rolls == []
		return total
	end

	if dice_rolls.count(1) > 0 && dice_rolls.count(1) < 3
		total = total + dice_rolls.count(1) * 100
	elsif dice_rolls.count(1) > 0 && dice_rolls.count(1) >= 3
		total = total + 1000 + (dice_rolls.count(1) - 3) * 100 
	end

	i=2
	while i < 6
		
		if dice_rolls.count(i) == 3
			total = total + i*100
		end

		i += 1
	end

    if dice_rolls.count(5) > 0
    	total = total + dice_rolls.count(5) * 50
    elsif dice_rolls.count(5) > 3
    	total = total + (dice_rolls.count(5) - 3) * 50
    end

	return total
end

puts score([])
puts score([1])
puts score([1,1])
puts score([1,1,1])
puts score([1,1,1,1])
puts score([5,2,3])
puts score([1,5,5,1,1])
puts score([1,1,3,3,3])