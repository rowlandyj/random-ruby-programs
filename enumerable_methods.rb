# 1. Define enumerable methods
=begin

map = similar to the .each enumerable, .map goes through each element of the list.  It is also
non destructive, but there is a destructive version called .map!.  This differs from .each
in that it returns a new array as opposed to the original array.

inject = a powerful enumerable that allows code to look more concise.  This enumerable returns the accumulated value of whatever was asked.  For
instance, using inject to find the sum of all the integers in a list.

select = similar to .each and .map this will go through each element of the list.  This in
particular, will pick out/select elements that meet the criteria given by the block. 	

=end
 
 
# -----------
# 2. Write your own method `Array#my_map`
#
# Here is a template to start with:
class Array
  def my_map
    shown = []
    
    0.upto(self.length-1) do |x|
      shown << yield(self[x])
    end
    
    shown
  end
end
 
puts [1,2,3,4].my_map { |i| i -= 1 }  == [0,1,2,3] # makes you count like a programmer!
 
# add your own test here
puts [1,3,5,6,8].my_map { |e| e>5 ? e : e+2  } == [3,5,7,6,8]