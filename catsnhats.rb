def catsnhats(num_cats, num_turns)
  cats = []
	
  num_cats.times do
	cats.push false
  end
  i = 1
	
  while i <= num_turns
	j = 0
		
	while j < cats.length
		  
	  if (j+1) % i == 0
        cats[j] = !cats[j]
      end

	  j += 1
	end

	i +=1
  end

  cats_with_hats=[]
  i=0
  
  while i < cats.length
    
    if cats[i] == true
    	cats_with_hats.<< (i+1)
    end
  	
  	i += 1
  end

  return cats.count(true), cats_with_hats.to_s
end

puts catsnhats(3, 3)
puts '------'
puts catsnhats(3, 1)
puts '------'
puts catsnhats(100,100)