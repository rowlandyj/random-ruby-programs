def prime_sum(max)
	require "mathn"
	primes = []

	primes = Prime.each(max) do |prime| primes << prime 
	end

	return primes.inject(0) {|sum, value| sum + value}
end

puts prime_sum(2000000)