# def bit_pos(num, pos)
#     return num & pos
# end

# puts bit_pos(86, 2) == bit_pos(86, 3)
# puts bit_pos(125, 1) == bit_pos(125, 2)
# puts bit_pos(389216,9) == bit_pos(389216,10)

def bit_val(num, p1, p2)
    return ((num >> (p1-1)) & 1) == ((num >> (p2-1)) & 1)
end

puts bit_val(244611,9,10)
puts bit_val(247917,15,16)
puts bit_val(86, 2, 3)
puts bit_val(125, 1, 2)