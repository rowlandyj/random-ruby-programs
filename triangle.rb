def triangle(a,b,c)
	sides = [a,b,c].sort
	
	if sides[0] <= 0
		return "Side lengths must be greater than 0"
	end

	if (sides[0] + sides[1]) <= sides[2]
		return "One side is too long or the same as the sum of other two"
	end

	if sides.uniq.length == 1 
		return "Equilateral Triangle"
	elsif sides.uniq.length == 2 
		return "Isosceles Triangle"
	elsif sides.uniq.length == 3 
		return "Scalene Triangle"
	end

end

puts triangle(3,2,1)
puts triangle(-1,2,1)
puts triangle(1,1,3)
puts triangle(1,1,1)
puts triangle(3,4,4)
puts triangle(3,4,3)
puts triangle(3,4,5)