bdays = {}

File.readlines('bdaylist.txt').each do |line|
	name, date, year = line.split(',')
	bdays[name] = Time.gm(year, *(date.split))
end

puts "Who's birthday do you want to know?"
name = gets.chomp
bday = bdays[name]

if bday == nil
	puts "Derno"
else
	now = Time.new
	age = now.year - bday.year

	if now.month > bday.month || (now.month == bday.month && now.day > bday.day)
		age += 1
	end

	if now.month == bday.month && now.day == bday.day
		puts "#{name} turns #{age} today!"
	else
		date = bday.strftime "%b %d"
		puts "#{name} will be #{age} on #{date}"
	end
end