def multiples? (n,max)
	(1..max).each { |x| if n % x != 0
						return false end	}
end

def range_p_factors (max)
	all_pfactors = []
	
	require "mathn"
	(1..max).each { |n| all_pfactors += n.prime_division }
	
	all_pfactors = all_pfactors.uniq

	pfactors = {}
	all_pfactors.each { |set| pfactors[set[0]] = set[1] }

	return pfactors
end

def smallest_multiple (max)
	prime_factors = []

	range_p_factors(max).each { |prime, occurence| occurence.times do 
												prime_factors << prime end}
	
	return prime_factors.inject { |result, element| result * element }
end

puts smallest_multiple (20)