def digit_roman_num(num)
    result = ""
    dic = 
        { 
            1000 => "M", 900 => "CM", 500 => "D", 400 => "CD", 100 => "C",
            90 => "LC", 50 => "L", 40 => "XL", 10 => "X", 9 => "IX", 5 => "V",
            4 => "IV", 1 => "I"
        }

    dic.each do |val, let|
        result += let * (num/val)
        num %= val
    end

    return result
end

puts digit_roman_num(1) == "I"
puts digit_roman_num(2) == "II"
puts digit_roman_num(4) == "IV"
puts digit_roman_num(9) == "IX"
puts digit_roman_num(10) == "X"
puts digit_roman_num(152) == "CLII"
puts digit_roman_num(3582) == "MMMDLXXXII"