# def in_words (num)
#   ones = %w(one two three four five six seven eight nine ten)
#   tens = %w(twenty thirty forty fifty sixty seventy eighty ninety)
#   teens = %w(ten eleven twelve thirteen fourteen fifteen sixteen seventeen eighteen nineteen)
#   big = %w(hundred thousand million billion trillion)
#   divisor = [1000, 1000000, 1000000000, 1000000000000]

#   num_word = ""
#   num_mag = num.to_s.length / 4
#   write = num
#   left = num
  
#   write = left / 1000000
#   if write > 0
#     num_word << in_words(write)
#     num_word << big[2] << " "
#     left = left - (write * 1000000) 
#   end

#   write = left / 1000
#   if write > 0
#     num_word << in_words(write)
#     num_word << big[1] << " "
#     left = left - (write * 1000) 
#   end

#   write = left / 100
#   if write > 0
#     num_word << in_words(write)
#     num_word << big[0] << " "
#     left = left - (write * 100)
#   end 

#   write = left / 10
#   if write > 1
#     num_word << tens[write - 2] << " "
#     left = left - (write * 10)
#   elsif write == 1
#     num_word << teens[left - 10] << " "
#     left = 0
#   end

#   write = left
#   if write > 0
#     num_word << ones[write - 1] << " "
#   end

#   return num_word
# end

def in_words(num)
  num_word = ""
  ones = %w(one two three four five six seven eight nine ten)
  tens = %w(twenty thirty forty fifty sixty seventy eighty ninety)
  teens = %w(ten eleven twelve thirteen fourteen fifteen sixteen seventeen eighteen nineteen)
  big = %w(hundred thousand million billion trillion)

  left = num

  if left == 1000000
    return "one million"
  end

  write = left/100000
  left = left - write * 100000
  if write > 0
    num_word << ones[write-1] << " " << big[0] << " "
    if left/10000 == 0
      num_word << big[1]
    end
  end

  write = left/10000
  left = left - write * 10000
  if write > 0
    if write == 1
      write = left/1000
      num_word << teens[write] << " " << big[1] << " "
      left = left - write *1000
    else
      num_word << tens[write-2] << " "
      if left/1000 == 0
        num_word << " " << big[1] << " "
      end
    end
  end

  write = left/1000
  left = left - write * 1000
  if write > 0
    num_word << ones[write-1] << " " << big[1] << " "
  end

  write = left/100
  left = left - write * 100
  if write > 0
    num_word << ones[write-1] << " " << big[0] << " "
  end

  write = left/10
  left = left - write * 10
  if write > 0

    if write == 1
      num_word << teens[left]
      left = 0
    else
      num_word << tens[write-2] << " "
    end

  end

  write = left
  if write > 0
    num_word << ones[write-1]
  end
  
  num_word
end

puts in_words(120)
puts in_words(310)
puts in_words(411)
puts in_words(512)
puts in_words(613)
puts in_words(814)
puts in_words(999)
puts in_words(999_999)
puts in_words(11_111)
puts in_words(22_222)
# puts in_words(1_111_111)
# puts in_words(999_999_999)
