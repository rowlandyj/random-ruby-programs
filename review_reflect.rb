# enumerable_methods
# https://gist.github.com/supertopher/1509f7606ce9ea0197e8
# https://gist.github.com/Tambling/2f0139d220f66626a225
# https://gist.github.com/cemmanuel1/0c09babff81ecdd7954b

#recursive_methods
# https://gist.github.com/jaredsmithse/0b3027211eb4b412eabc
# https://gist.github.com/syn-splendidus/2fd24fc7b2f32b9cf98a
# https://gist.github.com/laumontemayor/da1954a27e90d9610e62

# Question 1
# It was nice to see how other people approached the problem and how other people did the spacing
# to their code.  

# Question 2
# No questions really.  The methods we looked at this weekend were pretty straight forward.
# I was interested to see what they named their variables at time since I am unsure if I use bad variable names. 

#Question 3
# No dead ends.

# Sudoku Solver Attempt 2
# https://gist.github.com/rowlandyj/7a34e8a5008887730b3b

# **My first attempt is on a computer at school, the file didn't get updated in my dropbox properly so I'll
# upload that onto github as a new gist and post it here on monday morning.  