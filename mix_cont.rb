def mixed_cont(input)
    result = ""
    arr = input.chomp.split(",")
    words = []
    digits = []

    arr.each do |x|
        if x =~ /\d/
            digits << x 
        else
            words << x 
        end
    end

    result << words.join(",")

    if (words.length > 0) && (digits.length > 0)
        result << "|"
    end

    result << digits.join(",")
    return result
end

puts mixed_cont("8,33,21,0,16,50,37,0,melon,7,apricot,peach,pineapple,17,21")
puts mixed_cont("24,13,14,43,41")