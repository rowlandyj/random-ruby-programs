def separate_comma(number)
  number = number.to_s
  if number.length > 4
    i = 4
    while number.length >= i
      number.insert(-i, ',')
      i += 4
    end
  end

  return number
end

puts separate_comma(100)
puts separate_comma(0)
puts separate_comma(1000)
puts separate_comma(10000)
puts separate_comma(1000000000)