def sum_sq (max)
	(1..max).inject {|result, element| element**2 + result}
end

def sq_sum (max)
	((1..max).inject {|result, element| element + result})**2
end

def ss_diff (max)
	sq_sum(max)-sum_sq(max)
end

puts ss_diff(100)