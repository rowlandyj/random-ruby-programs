def tri_num(n)
	return (1..n).inject(0) { |sum, num| sum + num }
end

def factors(n)
	num_facts = 0
	i=1
	sqrt = (n**0.5).to_i
	
	while i <= sqrt
		if n%i==0
			num_facts += 2
		end
		i += 1
	end

	if sqrt**2 == n
		num_facts -= 1
	end

	return num_facts
end

def divisors(min)
	i = 1

	while factors(tri_num(i)) <= min
		i += 1
	end

	return tri_num(i)
end

print divisors(500)