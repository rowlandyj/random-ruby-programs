def simple_sort(input)
    arr = input.chomp.split(" ")
    arr = arr.map(&:to_f)
    result = arr.sort
    result = result.map { |x| sprintf("%0.3f", x) }
    return result.join(" ")
end

puts simple_sort("70.920 -38.797 14.354 99.323 90.374 7.581")
puts simple_sort("-37.507 -3.263 40.079 27.999 65.213 -55.552")
puts simple_sort("99.076 -95.753 13.170 56.419 0.221 -90.368 -33.912 -37.682 99.536 6.581 -60.099 51.183 -37.807 20.480 -28.012")
