def is_fibonacci?(i)
  fib = Hash.new {|h,k| k < 2 ? h[k] = k : h[k] = h[k-1] + h[k-2] } #using hash for fib is pretty awesome
  
  j=0
  
  while fib[j] < i
  	j += 1
  end
  
  return fib.has_value?(i)
end

puts is_fibonacci?(0)
puts '---'
puts is_fibonacci?(19810812)
puts '---'
puts is_fibonacci?(89)
puts '---'
puts is_fibonacci?(55)
puts '---'
puts is_fibonacci?(8)
