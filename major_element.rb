def major_el(input)
    arr = input.split(",")
    l = arr.length / 2
    major = "NONE"

    freq = arr.inject(Hash.new(0)) { |h, v| h[v] += 1; h}
    if freq.values.max > l
        major = freq.key(freq.values.max)
    end
    return major
end

puts major_el('92,19,19,76,19,21,19,85,19,19,19,94,19,19,22,67,83,19,19,54,59,1,19,19')
puts major_el('92,11,30,92,1,11,92,38,92,92,43,92,92,51,92,36,97,92,92,92,43,22,84,92,92')
puts major_el('4,79,89,98,48,42,39,79,55,70,21,39,98,16,96,2,10,24,14,47,0,50,95,20,95,48,50,12,42')