# ## List 10 topics

# 1) recursion
# 2) array enumerables
# 3) binary search
# 4) linear search
# 5) iteration vs recursion
# 6) regular expression
# 7) using "yield" for blocks
# 8) nested arrays
# 9) abstract data structures: stacks and queues 
# 10) psuedo coding



# ## Choose two and write a description

=begin
# 3) binary search: a powerful algorithm/approach to search through an assorted list.  
This method in most cases a more efficient way to find/search for things when compared 
to linear search (scanning through a list in order).  This utilizes the halfway point of 
the list.  The basic stratigy is if the midpoint is smaller than the target look at the 
3/4 pt (the halfway pt of the upper half of the list) of the list or if the midpoint is 
bigger look at the 1/4 pt (the halfway pt of the bottom half of the list) of the list.  
You continue this process until you find the target within the list or can conclude that 
the value does not exist in the list.  

# 10) recursion: This is when a method calls upon itself.  This is powerful in that it 
can shorten code and make it more readable.  However, in many cases this is less efficient 
than simply using iteration (loops).  Similar to loops, recursion requires a break point 
(base case) so that it can end or one can end with an infinite loop which tend to leads 
to stack overflow.  Classic examples of using recursion are fibonaaci and factorials  
=end

## Write code to illustrate one of your descriptions

def reverse_arr(arr)
	arr.empty? ? arr : [arr.pop, reverse_arr(arr)].flatten
end

p reverse_arr([1,2,3])
p reverse_arr([5,7,3,2])
