require 'date'

def work_exp(input)
    dates = date_parse(input)
end

def date_parse(input)
    arr = input.chomp.split("; ")
    dates = arr.map { |x| x.split("-") }

    dates.each_index do |x| 
        dates[x][0] = Date.parse(dates[x][0])
        dates[x][1] = Date.parse(dates[x][1])
    end

    dates.sort
end

def overlap(dates)
    result = []
end

puts work_exp("Feb 2004-Dec 2009; Sep 2004-Jul 2008")
puts work_exp("Aug 2013-Mar 2014; Apr 2013-Aug 2013; Jun 2014-Aug 2015; Apr 2003-Nov 2004; Apr 2014-Jan 2015")
puts work_exp("Mar 2003-Jul 2003; Nov 2003-Jan 2004; Apr 1999-Nov 1999")
puts work_exp('Apr 1992-Dec 1993; Feb 1996-Sep 1997; Jan 2002-Jun 2002; Sep 2003-Apr 2004; Feb 2010-Nov 2011')
puts work_exp("Feb 2004-May 2004; Jun 2004-Jul 2004")