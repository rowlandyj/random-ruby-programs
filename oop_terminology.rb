#Attr_reader, writer, or accessor
=begin 
  The attr_reader, attr_writer, and attr_accessor are accessors make it easier to write classes and help show your intent
of certain variables to someone who is reading your code.  

  Using attr_reader on a certain variable shows that you intend for the user of your program/class to be able to only "read" 
or get that variable.  Let us see an example of this in action.  
class Person
  attr_reader :name
  .
  .
  .
end
With this one line of code we are essentially including the method:
  def name
    @name
  end
Which will reveal the value of the instance variable @name.

  Similarly to the attr_reader, the attr_writer shows the intention of allowing others to "change" or set a particular variable.
class.  

class Person
  attr_writer :name
  .
  .
  .
end
With this line of code we are essentially including the method:
  def name=(val)
    @name = val
  end
Which allows for the value of @name to be changed.

Finally we have attr_accessor which is the combination of attr_reader and attr_writer.  When you use this you are essentially giving
certain variables full outside access (users or other methods/objects can change and read the value of these variables)

class Person
  attr_accessor :name
  .
  .
  .
end
Includes both the methods listed earlier.
=end

#Public and Private Methods
=begin 
  By default, all methods are private in ruby which means that anyone or anything can access it.  Private methods will not
be available for use from outside objects.  It should also be noted that private methods are only accessible in the same
instance of an object; it cannot just be the same class.  To set a method to private you simple write 'private' and everything
after this will become a private method unless you invoke 'public' later on.  
=end

#Instance and Class methods
=begin 
Instance methods, as the name suggest, are methods that can be called on by an instance of an object while Class methods can
be called on by a class (how insightful).  One common class method that we have been using is the .new method which we call upon
to make a new instance of a certain class.  Thus, class methods are methods are used for anything that deals with a class as a whole, not a single
instance.

The two different ways that a class method and instance method are done like so: 
class Example
  
  def self.method                       **There are other ways to define class methods but this is my favorite and probably the cleanest way to do it
    p 'this is a class method'
  end

  def method
    p 'this is an instance method'
  end
end
=end

#Instance and Class variables
=begin 
  Class variables are denoted as such: @@var.
  Instance variables are denoted as such: @var.
Class variables will be inherited by subclasses and superclass while instance variables are only inherited by the subclasses unless redefined.
Instance variables are can only be called upon/used when an instance of the class is used.  For example
class Blah
  attr_accessor :blah
  def initialize
    @blah = 2
  end
end

puts Blah.blah ==> NoMethodError
puts Blah.new.blah ==> 2
=end


#Separation of Concerns
=begin 
  This is a deisgn principle that focuses on separating responsibilities/tasks/concerns.  In a sense we are making our code very compartmentalized
by creating different objects and while this may seem tedious at first, it actually allows the code to be cleaner to read in the long run and makes 
it easier to make additions to.  Think of it as separating a room into multiple areas so that each area's activity will not affect the activity in another
area; the areas can communicate to one another but their activities should not be done in the same space.
**In short, this states we should break down the program into sections/parts and each part can communicate with each other if need be.  
=end


#Single Responsibility
=begin 
  Similar to separation of concerns and closely related to it, this design principle states that every object/class should only have one responsibility.  Objects should then be extremely
  specialized at what they do.  This is also similar to the principle that one method does only one thing.
**In short, each part of the program should only do one thing.  To meet the overall goal each part can work with other parts.  
=end

class VideoGameConsole #recieves msgs from controller, recieves and sends to game
  attr_accessor :player_1, :player_2

  def initialize(game=nil,controller)
    @game = game
    @controller = controller
    @power = false
  end

  def d_pad
    @controller.d_pad = true
  end

  def read_game
    return "insert game" if game.nil?
    @game
  end

  def a_button
    @controller.a_button
  end

  def b_button
    @constroller.b_button
  end

  def pause
    @controller.start_button
  end

  def select_button
    @controller.select_button
  end

  def turn_on
    @power = true
  end

  def turn_on
    @power = false
  end

  def choose_players
    @controller.select_button 
  end
end

class VideoGameController #sends msgs to console
  attr_accessor :a_button, :b_button, :select_button, :start_button, :d_pad

  def initialize(console)
    @a_button = console[:a_button]
    @b_button = console[:b_button]
    @select_button = console[:select_button]
    @start_button = console[:start_button]
    @dpad = console[:dpad]
  end

end

class VideoGame #recieves and sends msgs to console
  attr_accessor :player_1, :player_2
  attr_reader :game_sprites, :game_level
  
  def initialize(console)
    @console = console
    @player_1 = false
    @p1_score = 0
    @player_2 = false
    @p2_score = 0
    @game_start = false
  end

  def move
    @console.dpad
  end

  def start_game
    @game_start = true
  end

  def mode_select
    @console.select_button
  end

  def pause
    @console.pause
  end

  def jump
    @console.a_button
  end

  def action
    @console.b_button
  end

  def choose_players
    start_menu
    case @console.choose_players
    when 1
      @player_1 = true
    when 2
      @player_1 = true
      @player_2 = true
    end
  end

private

  def start_menu(@console.dpad,@console.a_button)
    puts "How many players?"
    return player_number
  end

end

