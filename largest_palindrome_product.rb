class String
	def palindrome?
		self == self.reverse
	end
end

def palindrome_prod(a,b)
	pp = a*b
	
	if pp.to_s.palindrome?
		return pp
	end

	return false
end

def trip_pp
  palindromes=[]
  i = 999
  while i > 100
  	j = 999
  	while j > 100
  		if palindrome_prod(i,j)
  			palindromes << palindrome_prod(i,j)
  		end
  		j -= 1
  	end
  	i -= 1
  end
  return palindromes.sort[-1]
end
puts palindrome_prod(11,11)
puts trip_pp