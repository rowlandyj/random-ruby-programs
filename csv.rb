require 'csv'
require 'date'
 
class Person
  attr_reader :id, :f_name, :l_name, :email, :phone, :created_at

  def initialize(row)
    @id = row[0]
    @f_name = row[1]
    @l_name = row[2]
    @email = row[3]
    @phone = row[4]
    @created_at = DateTime.parse(row[5])
  end

  def to_a
    [@id, @f_name, @l_name, @email, @phone, @creatd_at]
  end

end
 
class PersonParser
  attr_reader :file
  
  def initialize(file)
    @file = file
    @people = nil
  end
  
  def people
    # If we've already parsed the CSV file, don't parse it again.
    # Remember: @people is +nil+ by default.
    return @people if @people
    @people ||=[]
    CSV.foreach(@file) do |row|
      next if row[0] == 'id'
      @people << Person.new(row)
    end
    return @people
  end

  def add_person(info)
    @people << Person.new(info)
  end

  def save
    CSV.open(@file, "wb") do |csv|
      header = ['id', 'first_name', 'last_name', 'email', 'phone', 'created_at']
      csv << header
      @people.each do |row|
        csv << row.to_a
      end
    end
  end

end
 
parser = PersonParser.new('people.csv')
 
puts "There are #{parser.people.size} people in the file '#{parser.file}'."