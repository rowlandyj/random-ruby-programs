def natural_35 (max)
	sum=0
	
	(1...max).each { |x| if (x % 3 == 0 || x % 5 == 0) 
		                    sum += x end}
	return sum
end

puts natural_35(1000)