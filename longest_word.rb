def longest_word(str)
    longest = ""
    str.split(" ").each { |word| longest = word if word.length > longest.length }
    return longest
end

puts longest_word("some line with text")
puts longest_word("another line")