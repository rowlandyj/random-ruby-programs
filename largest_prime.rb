def primes(n)
  return [] if n == 1
  factor = (2..n).find {|x| n % x == 0}
  [factor] + primes(n / factor)
end

puts primes (600851475143)
puts'--------'
puts primes (45)