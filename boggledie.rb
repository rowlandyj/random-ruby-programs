class Die
  def initialize(labels)
    if labels.length == 0  
    	raise ArgumentError.new("Need Side Values")
    end
    @sides = labels.length
    @die = labels
  end

  def sides
    @sides
  end

  def roll
  	@die.sample
  end
end

die = Die.new(['A', 'B', 'C', 'D', 'E', 'F'])
puts die.sides # still returns the number of sides, in this case 6
puts die.roll # returns one of ['A', 'B', 'C', 'D', 'E', 'F'], randomly