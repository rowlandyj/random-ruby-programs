def hidden_dig(str)
    result = ""
    dic = {
                'a' => 0,
                'b' => 1,
                'c' => 2,
                'd' => 3,
                'e' => 4,
                'f' => 5,
                'g' => 6,
                'h' => 7,
                'i' => 8,
                'j' => 9
            }

    str.each_char do |char|
        if char =~ /\d/
            result << char
        elsif dic[char] != nil
            result << dic[char].to_s
        end
    end

    result.length == 0 ? 'NONE' : result
end

puts hidden_dig('abcdefghik')
puts hidden_dig('12345b')
puts hidden_dig('yt')