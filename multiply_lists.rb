def list_prod(input)
    arr = input.chomp.split(" | ")
    list1 = get_list(arr[0])
    list2 = get_list(arr[1])
    result = []

    list1.each_index do |i|
        result << (list1[i] * list2[i])
    end
    result.join(" ")
end

def get_list(arr)
    list = arr.split(" ")
    list = list.map(&:to_i)
end

puts list_prod("9 0 6 | 15 14 9") == [135, 0, 54]
puts list_prod("5 | 8") == [40]
puts list_prod("13 4 15 1 15 5 | 1 4 15 14 8 2") == [13, 16, 225, 14, 120, 10]
puts list_prod("48 38 0 92 5 44 41 30 | 49 31 48 99 59 14 98 51")