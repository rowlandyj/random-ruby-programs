# class ThreebyThree
# 	attr_reader :threebythree

# 	def initialize(sudoku_chunk)
# 		@sudoku_chunk = sudoku_chunk
# 		@threebythree = []
# 	end

# 	def proc_threebythree
# 		@sudoku_chunk.each { |e| @threebythree << [e] }
# 		return @threebythree
# 	end

# 	def empty_square?
# 		empty_square = false
# 		@sudoku_chunk.each do |e|
# 			e.each_char { |char| empty_square = true if char == "0" }
# 		end
# 		return empty_square
# 	end

# 	def repeated_values?
# 		grid_values = [] 
# 		@sudoku_chunk.each do |e|
# 			e.each_char { |char| grid_values << char.to_i }
# 		end
		
# 		if grid_values.uniq.length == 9 
# 			return true
# 		end

# 		false 
# 	end
# end

class Sudoku
  def initialize(board_string)
  	@board = board_string
  	@solved_board = ''
  	@rows = []
  	@columns = []
  	@grids = []
  end
 
  def solve!
  	poss_answer = (1..9).to_a.map! { |e| e.to_s }
  	total_blanks = @board.count('0')
  	answer = ''
  	
  	while total_blanks > 0 
  		i = 0
  		
  		
  		while i < 81
	  		given_nums = []
	  		answer = ''
	  	
	  		if @board[i] == '0'
	  			given_nums = col_return(i).split('').uniq + row_return(i).split('').uniq + grid_return(i).split('').uniq
	  			given_nums.uniq! - ['0'] 
	  			answer = poss_answer-given_nums

	  			if answer.length == 1
	  				@board[i] = answer[0]
	  			end
	  		end
	  		i += 1 
	  	end

	  	new_blanks = @board.count('0')

	  	if new_blanks == total_blanks
	  		return @solved_board = "Puzzle is too hard"
	  	else
	  		total_blanks = new_blanks
	  	end
  		
  	end

  	return @board
  end
 
  # Returns a string representing the current state of the board
  # Don't spend too much time on this method; flag someone from staff
  # if you are.
  def board
  	return puts @solved_board if !@solved_board.empty?
  	board = @board.split('').each_slice(3).to_a
  	i = 1
  	j = 1
  	puts '------------'
  	while i <= board.length
  		print board[i-1].join('') + "|"
  		
  		if i%3 == 0
  			print "\n"
  			j += 1
  		end

  		if j == 4
  			puts '------------'
  			j = 1
  		end 
  		
  		i += 1
  	end 
  end

  def grids
  	i = 0
  	while @grids.length < 9
  		j = 0
  		curr_grid = []
  		
  		if @grids.length == 3
  			i = 9
  		elsif @grids.length == 6
  			i = 18
  		end

  		# if @grids.length % 3 == 0
  		# 	i += 7
  		# end
  		
  		while j < 7
  			curr_grid << @board.split('').each_slice(3).to_a[j+i].join
  			j += 3  		
  		end
  		
  		@grids << curr_grid.join('')
  		i += 1
  	end

  	return @grids
  end

  def rows
  	@rows = @board.scan(/\d{9}/)
  	return @rows
  end

  def columns
  	i = 0
  	while i < 9
  		j = 0
  		curr_col = ""
  		while j < 80
  			curr_col << @board[j+i]
  			j += 9
  		end
  		@columns << curr_col
  		i += 1
  	end

  	return @columns
  end

  def row_return (index)
  	rows
  	case index
  	when 0..8
  		@rows[0]
  	when 9..17
  		@rows[1]
  	when 18..26
  		@rows[2]
  	when 27..35
  		@rows[3]
  	when 36..44
  		@rows[4]
  	when 45..53
  		@rows[5]
  	when 54..62
  		@rows[6]
  	when 63..71
  		@rows[7]
  	when 72..80
  		@rows[8] 
  	end
  end

  def col_return (index)
  	columns
  	case index
  	when 0,9,18,27,36,45,54,63,72
  		@columns[0]
  	when 1,10,19,28,37,46,55,64,73
  		@columns[1]
  	when 2,11,20,29,38,47,56,65,74
  		@columns[2]
  	when 3,12,21,30,39,48,57,66,75
  		@columns[3]
  	when 4,13,22,31,40,49,58,67,76
  		@columns[4]
  	when 5,14,23,32,41,50,59,68,77
  		@columns[5]
  	when 6,15,24,33,42,51,60,69,78
  		@columns[6]
  	when 7,16,25,34,43,52,61,70,79
  		@columns[7]
  	when 8,17,26,35,44,53,62,71,80
  		@columns[8] 
  	end
  end

  def grid_return (index)
  	grids
  	case index
  	when (0..2), (9..11), (18..20)
  		@grids[0]
  	when (3..5), (12..14), (21..23)
  		@grids[1]
  	when (6..8), (15..17), (24..26)
  		@grids[2]
  	when (27..29), (36..38), (45..47)
  		@grids[3]
  	when (30..32), (39..41), (48..50)
  		@grids[4]
  	when (33..35), (42..44), (51..53)
  		@grids[5]
  	when (54..56), (63..65), (72..74)
  		@grids[6]
  	when (57..59), (66..68), (75..77)
  		@grids[7]
  	when (60..62), (69..71), (78..80)
  		@grids[8]	
  	end
  end
end

# grid = ThreebyThree.new(["123","456","789"])
# grid.proc_threebythree
# p grid.threebythree
# p grid.empty_square?
# p grid.repeated_values?

unsolved_sudoku =  '003020600900305001001806400008102900700000008006708200002609500800203009005010300'

sudoku = Sudoku.new(unsolved_sudoku)
# sudoku.board

# puts sudoku.row_return(4)
# puts sudoku.columns
# puts sudoku.grids.length
# p unsolved_sudoku.length
sudoku.solve!
sudoku.board