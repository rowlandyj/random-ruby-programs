def shortest_rep(input)
    count = 0
    arr = input.chomp.split("")
    first = arr[0]
    arr.each_index do |i|
        if  i !=0 && arr[i] == first
            break
        end
        count += 1
    end
    return count
end

puts shortest_rep('ddddddddd') == 1
puts shortest_rep('abcabcabcabc') == 3