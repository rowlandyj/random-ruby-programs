def super_fizzbuzz(array)
  array.map! {|x| if x % 3==0 && x % 5==0
                     x = "FizzBuzz"
                  elsif x % 3 == 0
                     x = "Buzz"
                  elsif x % 5 == 0
                     x = "Fizz"
                 else
                 	x
                  end }
  return array
end

puts super_fizzbuzz([1,2,3,4,5,15])