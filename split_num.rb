def split_num(input)
    arr = input.chomp.split(" ")
    nums = arr[0]
    eval = arr[1]
    dic = let_num_conv(nums, eval)
    eval_str(dic,eval)
end

def let_num_conv(num, eval)
    dic = {}
    i = 0
    eval.each_char do |x|
        if x =~ /[+-]/
            next
        end
        dic[x] = num[i]
        i += 1
    end
    dic
end

def eval_str(dic, str)
    num1 = ""
    num2 = ""
    op = ""
    left_half = true

    str.each_char do |x|
        if x =~ /[+-]/
            left_half = false
            op << x
            next
        end
        if left_half
            num1 << dic[x]
        else
            num2 << dic[x]
        end
    end

    if op == "+"
        num1.to_i + num2.to_i
    else
        num1.to_i - num2.to_i
    end
end


puts split_num("3413289830 a-bcdefghij") == -413289827
puts split_num("776 a+bc") == 83
puts split_num("12345 a+bcde") == 2346
puts split_num("1232 ab+cd") == 44
puts split_num("90602 a+bcde") == 611