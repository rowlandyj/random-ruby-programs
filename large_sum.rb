def call_num(file_name)
	file = open(file_name)
	number = file.read
ensure
	file.close if file
	return number
end

def large_sum(file_name)
	number = call_num(file_name)

	return number.each_line.inject(0){ |sum, x| sum = sum + x.to_i}.to_s[0..9]
end

print large_sum("bigassnumber.txt")