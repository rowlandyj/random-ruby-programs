class Array
  def pad!(min_size, value = nil)
    until self.length == min_size do self.push(value) end
    return self
  end
  
  def pad(min_size, value = nil)
    padding = [value]*(min_size-self.length)
    self + padding
  end
end

arr=[1,2,3]
puts arr.pad(5,'blah')
puts '----'
puts arr 