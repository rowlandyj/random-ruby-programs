require 'json'

def json_menu_id(input)
    parsed = JSON.parse(input.chomp)
    result = 0

    parsed["menu"]["items"].compact.each do |x|
        if x["label"] 
            result += x["id"]
        end
    end

    return result 
end

puts json_menu_id('{"menu": {"header": "menu", "items": [{"id": 27}, {"id": 0, "label": "Label 0"}, null, {"id": 93}, {"id": 85}, {"id": 54}, null, {"id": 46, "label": "Label 46"}]}}')
puts json_menu_id('{"menu": {"header": "menu", "items": [{"id": 81}]}}')
puts json_menu_id('{"menu": {"header": "menu", "items": [{"id": 70, "label": "Label 70"}, {"id": 85, "label": "Label 85"}, {"id": 93, "label": "Label 93"}, {"id": 2}]}}')