#Author: Brian Mathew's
#Source: https://gist.github.com/byee322/fd59cf77709302d5dd82
# def to_roman(num)
#   numeral = ""
 
#   divisor = { 
#     1000 => "M" ,
#     500 => "D",
#     100 => "C",
#     99 => "XCIX",
#     90 => "XC",
#     50 => "L",
#     49 => "XLIX",
#     40 => "XL",
#     10 => "X",
#     9 => "IX",
#     5 => "V",
#     4 => "IV",
#     1 => "I"}
 
 
#     divisor.each do |arabics, romans|
#       # p "arab = #{arabics}, romans = #{romans}"
 
#       if num >= arabics 
#         number_of_romans = num / arabics
#         numeral << (romans * number_of_romans)
#         num -= arabics * number_of_romans
#       end
#       num
#     end
 
 
#     numeral
# end

# #tests
# p to_roman(1) == 'I' #true
# p to_roman(4) == "IV" #true
# p to_roman(44) == "XLIV" #true
# p to_roman(900) == "CM" #true but is false because that was defined wrong in the hash


#Explanation
=begin
 
The code uses a Hash that acts as a translator for arabic numubers to (modern) roman numerals.
The output, numeral, is initialized as an empty string.
The input, integer, is process after the hash where it is divided by the arabic numbers in the hash
so that the appropriate roman numeral will be appended to the output string.  The input integer is 
is also reduced with each run of the iteration process until it is fully process (=0).
The last line of code then ensures that the roman numeral string is returned.  

=end

#Psuedo code

=begin
Input: Integer (num)
Output: String (roman_num)

Consider what are the special cases of translating arabic to (modern) roman:
  4 = IV, 9 = IX, and so forth

What data structure should we use?
Hash since we can use the |key,value| notation.  key = arabic, value = roman

How do we ensure that we have the proper roman translation?
Check num by the key in the Hash.  To process the num we divide the num
by the arabic values stored in the Hash.  **Need biggest key to be on top of Hash.

=end

def to_roman(num)
    numeral = ""
 
  divisor = { 
    1000 => "M" ,
    900 => "CM",
    500 => "D",
    100 => "C",
    90 => "XC",
    50 => "L",
    40 => "XL",
    10 => "X",
    9 => "IX",
    5 => "V",
    4 => "IV",
    1 => "I"}
 
 
    divisor.each do |arabics, romans|
        number_of_romans = num / arabics
        numeral << (romans * number_of_romans)
        num %= arabics
    end
 
 
    numeral
end

p to_roman(1) == 'I' #true
p to_roman(4) == "IV" #true
p to_roman(44) == "XLIV" #true
p to_roman(99) == "XCIX"
p to_roman(900) == "CM" #true but is false because that was defined wrong in the hash

#Changes
=begin 
**added a missing comma (see source gist for this error i fixed it earlier as well to run the test code) to has and added 900 => to the hash
1) got rid of redundant values in hash, 99 and 49.
2) took out unnecessary if statement and used %= instead of -=
=end
